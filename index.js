const express = require('express');
const jwt = require('jsonwebtoken');
const path = require('path');
const multer = require('multer');
const fetch = require('node-fetch');
const { encode } = require('punycode');

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static('upload'));

// Replace this with your own secret key
const secretKey = '42629257EE6E7A86974F236A3373A';

// In-memory storage for registered users (for demo purposes)
const users = [];

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'upload');
  },
  filename: function (req, file, cb) {
    const uniqueSuffix = Date.now();
    cb(null, file.fieldname + '-' + uniqueSuffix + '-' + file.originalname);
  }
});

const upload = multer({ storage });

let token = null;

// Middleware to authenticate JWT token
function authenticateToken(req, res, next) {
  // const token = req.header('Authorization');
  if (!token) {
    return res.status(401).json({ message: 'Authentication token missing' });
  }

  jwt.verify(token, secretKey, (err, decoded) => {
    if (err) {
      return res.status(403).json({ message: 'Invalid token' });
    }
    req.user = decoded;
    next();
  });
}

// POST /register - User registration
app.post('/register', (req, res) => {
  const { username, password } = req.body;
  if (!username || !password) {
    return res
      .status(400)
      .json({ message: 'Username and password are required' });
  }

  // Create user object and store it in the users array
  const user = { username, password };
  users.push(user);

  res.status(201).json({ message: 'User registered successfully' });
});

// POST /login - User login and JWT token generation
app.post('/login', (req, res) => {
  const { username, password } = req.body;
  if (!username || !password) {
    return res
      .status(400)
      .json({ message: 'Username and password are required' });
  }

  // Check if user exists and password matches
  const user = users.find(
    (u) => u.username === username && u.password === password
  );
  if (!user) {
    return res.status(401).json({ message: 'Invalid credentials' });
  }

  // Generate JWT token
  const token = jwt.sign({ username }, secretKey, { expiresIn: '1h' });
  res.json({ token });
});

// POST /uploadImage - Upload an image (authentication required)
app.post(
  '/uploadImage',
  authenticateToken,
  upload.single('image'),
  (req, res) => {
    // You can implement image upload logic here
    // For demonstration purposes, let's assume the user is authorized to upload

    if (!req.file) {
      return res.status(400).send('No image provided.');
    }

    // res.status(200).send('Image uploaded successfully.');
    res.redirect('/' + req.file.filename);
  }
);

// render html
app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname) + '/public/login.html');
});

app.get('/signup', (req, res) => {
  res.sendFile(path.join(__dirname) + '/public/register.html');
});

app.post('/doSignup', async (req, res, next) => {
  const { username, password } = req.body;
  console.log('username: ', username);
  console.log('password: ', password);

  let body = {
    username: username,
    password: password
  };

  let resp = await fetch('http://localhost:3000/register', {
    method: 'POST',
    body: JSON.stringify(body),
    headers: { 'Content-Type': 'application/json' }
  });
  if (resp.ok) {
    res.redirect('/');
  } else {
    let error = await resp.json();
    res.send('Regiser Error: ' + error.message);
  }
});

app.post('/doLogin', async (req, res, next) => {
  const { username, password } = req.body;
  console.log('username: ', username);
  console.log('password: ', password);

  let body = {
    username: username,
    password: password
  };

  let resp = await fetch('http://localhost:3000/login', {
    method: 'POST',
    body: JSON.stringify(body),
    headers: { 'Content-Type': 'application/json' }
  });
  if (resp.ok) {
    let data = await resp.json();
    token = data.token;
    console.log('Token: ' + token);
    res.redirect('/upload');
  } else {
    let error = await resp.json();
    res.send('Login Error: ' + error.message);
  }
});

app.get('/upload', (req, res) => {
  res.sendFile(path.join(__dirname) + '/public/upload.html');
});

const PORT = 3000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
