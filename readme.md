## How to run the project
```bat
npm install
```

```bat
npm start
```

## API Guide
>
> POST /register
- Register user with username and password

>
> POST /login
- Login user with username and password

>
> POST /uploadImage
- Upload and display uploaded image with authentication required

## Usage Guide
1. Go to [index](http://localhost:3000) page
2. Click on [signup](http://localhost:3000/signup) link
3. Register with username and password
4. Login with registered username and password
5. Upload image on [upload](http://localhost:3000/upload) page